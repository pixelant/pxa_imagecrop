<?php
namespace Pixelant\PxaImagecrop\ContentObject;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

class ContentObjectRenderer extends \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer {

	/**
	 * This class extends TYPO3 core built in ContentObjectRenderer getImageResource class.
	 * It checks if the two extended fields in sys_file_reference is set, and if, tries to calculate crop settings.
	 * When cropsettings are calculated, it adds them to the ts params array, and continues to the "original" function with the new parameters included.
	 *
	 * @param string|\TYPO3\CMS\Core\Resource\File|\TYPO3\CMS\Core\Resource\FileReference $file A "imgResource" TypoScript data type. Either a TypoScript file resource, a file or a file reference object or the string GIFBUILDER. See description above.
	 * @param array $fileArray TypoScript properties for the imgResource type
	 * @return array|NULL Returns info-array
	 * @see IMG_RESOURCE(), cImage(), \TYPO3\CMS\Frontend\Imaging\GifBuilder
	 */
	public function getImgResource($file, $fileArray) {
		
		if ( !empty($fileArray) && is_array($fileArray) > 0 )  {

			if ( !empty($this->currentFile) ) {
				// Fetch merged properties
				$fileReferenceMergedProperties = $this->currentFile->getProperties();
					// if crop_ratio and crop_data is set
				if ( false === empty($fileReferenceMergedProperties['crop_ratio']) && false === empty($fileReferenceMergedProperties['crop_data']) ) {
						// list ratio to variables
					list($aspectRatioWidth,$aspectRatioHeight) = explode(":", $fileReferenceMergedProperties['crop_ratio']);
						// if aspectratios are set
					if( (integer)$aspectRatioWidth > 0 && (integer)$aspectRatioHeight > 0) {
							// calculate variables and add them to  params
						$factor = $aspectRatioWidth / $aspectRatioHeight;
						list( $cropWidth, $cropX, $cropY ) = explode("|", $fileReferenceMergedProperties['crop_data']);
						$width = $this->getWidth($cropWidth, $fileArray);
						$height = ceil($width / $factor);
						$cropHeight = ceil($cropWidth / $factor);
							// Add params to ts, include present params after so crop and resize gets first
						$fileArray['params'] = " -crop " . $cropWidth . "x" . $cropHeight ."+" . $cropX . "+" . $cropY . "  -resize " . $width . "x" . $height ." " . $fileArray['params'];

							// unset unwanted settings					
						unset($fileArray['width.']);
						unset($fileArray['maxH']);
						unset($fileArray['maxW']);
						unset($fileArray['width']);
						unset($fileArray['height']);
					}
				}
			}
		}

		return parent::getImgResource($file, $fileArray);
	}	

	/**
	 * Calculates width of the element by checking parameters in a certain order.
	 * 
	 * 1: maxW (passed from ts content settings "styles.content.imgtext.maxW" and "styles.content.imgtext.maxWInText")
	 * 2: width (passed from ts content settings "styles.content.imgtext.linkWrap.width") and if not larger than maxW
	 * 3: width field (ex. tt_content 'imagewidth') and if not larger than maxW
	 * 4: actual crop with
	 * 5: with, and array bodyTag is set (probably click enlarge)
	 * 
	 * @param integer @cropWidth The width of the "cropped" area 
	 * @param array $fileArray TypoScript properties for the imgResource type
	 * @param double $factor Aspect ration (aspectRatioWidth / aspectRatioHeight)
	 * @return integer Returns the width to use
	 */
	private function getWidth($cropWidth, $fileArray) {

		$width = 0;
		$tsWidth = (integer)$fileArray['width'];
		$tsMaxWidth = (integer)$fileArray['maxW'];
		$fieldWidth = isset($this->data[$fileArray['width.']['field']]) ? (integer)$this->data[$fileArray['width.']['field']] : 0;
		$tsClickEnlarge = isset($fileArray['bodyTag']) ? true : false;

		if ( $tsMaxWidth > 0 ) {
			$width = $tsMaxWidth;
		}

		if ( $tsWidth > 0 && $tsWidth < $tsMaxWidth ) {
			$width = $tsWidth;
		}

		if ( $fieldWidth > 0 && $fieldWidth < $tsMaxWidth ) {
			$width = $fieldWidth;
		}

		if ( $width == 0 ) {
			$width = $cropWidth;
		}

		if ( $tsWidth > 0 && $tsClickEnlarge ) {
			$width = $tsWidth;
		}

		return $width;

	}
}
?>