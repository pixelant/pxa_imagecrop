<?php
namespace Pixelant\PxaImagecrop\UserFunction;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Pixelant AB
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
/**
 * Renders
 *
 * @package	PxaImagecrop
 * @subpackage UserFunction
 */
class SysFileReferenceCrop {

    /**
     * Array to store typoscript configuration for pxa_generic_content
     * @var array
     */
    protected $typoScriptSetup;

    /**
     * Array to store messages
     * @var array
     */
    protected $messages;

    /**
     * @var int The uid of the content
     */
    protected $uid;

    /**
     * @var int The width of the preview crop image in flexform
     */
    protected $previewImageWidth;

    /**
     * @var int The width of the original image
     */
    protected $originalImageWidth;

    /**
     * @var int The height of the original image
     */
    protected $originalImageHeight;

    /**
     * @var int The image scale factor
     */
    protected $imageScaleFactor;
    
    /**
     * @var string The aspect ratio string :
     */
    protected $aspectRatio;
    
    /**
     * @var int The aspect ratio width
     */
    protected $aspectRatioWidth;
    
    /**
     * @var int The aspect ratio height
     */
    protected $aspectRatioHeight;

    /**
     * @var int The cropY
     */
    protected $cropY;

    /**
     * @var int The cropX
     */
    protected $cropX;

    /**
     * @var int The cropY2
     */
    protected $cropY2;

    /**
     * @var int The cropX2
     */
    protected $cropX2;

    /**
     * @var string The actual content
     */
    protected $content;

	/**
	 * @param array $parameters
	 * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
	 * @return string
	 */
	public function renderField(array &$parameters,\TYPO3\CMS\Backend\Form\FormEngine &$pObj) {
        
        if ( empty($parameters['row']['crop_ratio']) ) {
            $this->messages[] = "Crop isn't enabled";
            return $this->getMessages();
            // get typoscript configuration
        }
        $this->getTypoScriptConf();

            // get file reference
        $fileReferenceObject = ResourceFactory::getInstance()->getFileReferenceObject(abs($parameters['row']['uid']));
        $fileObject = $fileReferenceObject->getOriginalFile();
 
        if ($fileObject->isMissing()) {
            $this->messages[] = "File is missing";
            return $this->getMessages();
        }

            // Adds out js and css to form
        $this->addStylesAndJavascriptToForm($pObj);

            // create input
        $this->content = $this->getFormInput($pObj, $parameters);

            // initialize parameters
        $this->uid = $parameters['row']['uid'];
        $this->originalImageWidth = $fileReferenceObject->getProperty('width');
        $this->originalImageHeight = $fileReferenceObject->getProperty('height');

        $this->previewImageWidth = isset($typoScriptSetup['previewImageWidth']) ? $typoScriptSetup['previewImageWidth'] : '612';
        
        $this->imageScaleFactor = $this->previewImageWidth / $this->originalImageWidth;

        $this->setAspectRatioAndFactor($parameters);
        
        $this->setCroppingValues($parameters);

        
        // Web image
        if (GeneralUtility::inList($GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'], $fileReferenceObject->getExtension())) {
            $imageUrl = $fileObject->process(ProcessedFile::CONTEXT_IMAGEPREVIEW, array(
                'width' => $this->originalImageWidth * $this->imageScaleFactor,
                'height' => $this->originalImageHeight * $this->imageScaleFactor,
            ))->getPublicUrl(TRUE);
            $this->content .= '<img id="jcrop_target_' . $this->uid . '" src="' . $imageUrl . '" alt="' . htmlspecialchars($fileReferenceObject->getName()) . '" />';
        }  
        
        // $this->content .= $this->getDetailedData();

        $this->addJavaScriptToContent();
        return $this->content;
	}

    /**
    * Sets the cropping values
    * 
    * @param array $parameters
    * @return void
    *
    */
    protected function setCroppingValues($parameters) {
            // Calculate x,y,x2 and y2 for the jCrop:  top, left, width, height
        list($cropWidth,$cropX,$cropY) = explode('|', $parameters['row']['crop_data']);
 
        $this->cropX = intval($cropX * $this->imageScaleFactor);
        $this->cropX2 = intval(($cropX + $cropWidth) * $this->imageScaleFactor);
        $this->cropY = intval($cropY * $this->imageScaleFactor);
        $this->cropY2 = $this->cropY + (ceil($cropWidth * $this->imageScaleFactor) / $this->factor);

    }

    /**
    * Sets aspect ratio and factor
    * 
    * @param array $parameters
    * @return void
    *
    */
    protected function setAspectRatioAndFactor($parameters) {

            // Fetch flexform aspectration
        $this->aspectRatio = str_replace(":", "/", $parameters['row']['crop_ratio']);
        list($this->aspectRatioWidth,$this->aspectRatioHeight) = explode(":", $parameters['row']['crop_ratio']);

            // Calculate aspectRatio factor from supplied aspect ratio
        if($this->aspectRatioWidth > 0 && $this->aspectRatioHeight > 0) {
            $this->factor = $this->aspectRatioWidth / $this->aspectRatioHeight;
        }       
    }

    /**
    * Adds js to content variable
    * @return void
    *
    */
    protected function addJavaScriptToContent() {
    
            // Add the javascript for jcrop, create object, onchange function and update form inputs.
        $this->content.= "
        <script type=\"text/javascript\">
            var factor_" . $this->uid . " = " . $this->imageScaleFactor . ";
            TYPO3.jQuery(document).ready(function() {

                TYPO3.jQuery('#jcrop_target_" . $this->uid . "').Jcrop({
                    onChange:    showCoords_" . $this->uid . ",
                    bgColor:     'black',
                    bgOpacity:   .4,
                    setSelect:   [ " . $this->cropX . "," . $this->cropY . "," . $this->cropX2 . "," . $this->cropY2 . "],
                    aspectRatio: " . $this->aspectRatio . "
                });
                function showCoords_" . $this->uid . "(c)
                {
                    // variables can be accessed here as c.x, c.y, c.x2, c.y2, c.w, c.h
                    TYPO3.jQuery('[name=\"data[sys_file_reference][" . $this->uid . "][crop_data]\"]').val(parseInt(c.w / factor_" . $this->uid . ") + '|' + parseInt(c.x / factor_" . $this->uid . ") + '|' + parseInt(c.y / factor_" . $this->uid . "));
                };
            });
        </script>";     
    }

    /**
     * Fetch TypoScript setup and store in property
     *
     */
    protected function getTypoScriptConf() {
            // Create ObjectManager
        $this->objectManager = GeneralUtility::makeInstance('Tx_Extbase_Object_ObjectManager');
            // Configuration manager
        $configurationManager = $this->objectManager->get('\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
            // full typoscript (couldn't get Tx_Extbase_Configuration_ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS to work....)
        $typoScriptSetupFull = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
            // add settings to array with the settings we need
        $this->typoScriptSetup = $typoScriptSetupFull['plugin.']['tx_pxaimagecrop.']['settings.']['crop.'];
            // unset and unset all
        unset($typoScriptSetupFull);
    }

    /**
     * Adds and css and javascript to BE form.
     *
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     * @return void
     */
    protected function addStylesAndJavascriptToForm(\TYPO3\CMS\Backend\Form\FormEngine &$pObj) {

        $extJquery = '<script type="text/javascript">
                        var jQuery = jQuery || TYPO3.jQuery;
                    </script>';
        $extCssRel = ExtensionManagementUtility::extRelPath('pxa_imagecrop') . 'Resources/Public/Css/Backend/jquery.Jcrop.min.css';
        $extJsRel = ExtensionManagementUtility::extRelPath('pxa_imagecrop') . 'Resources/Public/Js/Backend/jquery.Jcrop.min.js';
            // Add stylesheets and javascript to BE form
        $pObj->additionalCode_pre = array(
            'jqForCrop' => $extJquery,
            'jqCropCss' => '<link rel="stylesheet" href="' . $extCssRel . '" />',
            'jqCropJs' => '<script src="' . $extJsRel . '" type="text/javascript"></script>',
        );
        
    }

    /**
     * Returns messages as html
     *
     * @return string Returns messages as html.
     */
    protected function getMessages() {
        $output = '';
        if (count($this->messages) > 0) {
            $output = '<ul class="messages">';
            foreach ($this->messages as $key => $message) {
                $output .= '<li>' . $message . '</li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    /**
     * Returns the html output for the input
     *
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     * @param array $parameters The parameters array
     * @return string
     */
    protected function getFormInput(\TYPO3\CMS\Backend\Form\FormEngine &$pObj, $parameters) {
        $output = '<div class="cropImage" style="display:none;">';
        $output .= $pObj->getSingleField_typeInput(
            $parameters['table'],
            $parameters['field'],
            $parameters['row'],
            $parameters
        );
        $output .= '</div>';
        return $output;
    }

    protected function getDetailedData() {

        $content = '<p>ratio = ' . $parameters['row']['crop_ratio'] . '</p>';
        $content .= '<p>uid = ' . $this->uid . '</p>';
        $content .= '<p>originalImageWidth = ' . $this->originalImageWidth . '</p>';
        $content .= '<p>originalImageHeight = ' . $this->originalImageHeight . '</p>';
        $content .= '<p>previewImageWidth = ' . $this->previewImageWidth . '</p>';
        $content .= '<p>imageScaleFactor = ' . $this->imageScaleFactor . '</p>';
        $content .= '<p>aspectRatioWidth = ' . $this->aspectRatioWidth . '</p>';
        $content .= '<p>aspectRatioHeight = ' . $this->aspectRatioHeight . '</p>';
        $content .= '<p>factor = ' . $this->factor . '</p>';
        $content .= '<p>cropY = ' . $this->cropY . '</p>';
        $content .= '<p>cropX = ' . $this->cropX . '</p>';
        $content .= '<p>cropX2 = ' . $this->cropX2 . '</p>';
        $content .= '<p>cropY2 = ' . $this->cropY2 . '</p>';
        
        return $content;

    }
}
?>
