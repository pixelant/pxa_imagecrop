<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$additionalSysFileReferenceColumns = array(
	'crop_ratio' => array(
		'exclude' => 0,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'Ratio',
		'onChange' => 'reload',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('Crop disabled', ''),
				array('2.39:1', '2.39:1'),
				array('2:1', '2:1'),
				array('16:9', '16:9'),
				array('8:5', '8:5'),
				array('3:2', '3:2'),
				array('4:3', '4:3'),
				array('1:1', '1:1'),
				array('3:4', '3:4'),
				array('2:3', '2:3'),
				array('1:2', '1:2'),
			),
			'size' => 1,
			'max' => 1,
		)
	),
	'crop_data' => array(
		'exclude' => 0,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'Crop',
		'readOnly' => 1,
		'displayCond' => array(
        	'AND' => array(
                'FIELD:crop_ratio:REQ:true',
        	),
		),
		'config' => array(
			'type' => 'user',
			'userFunc' => 'Pixelant\PxaImagecrop\UserFunction\SysFileReferenceCrop->renderField',
		)
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $additionalSysFileReferenceColumns);

	// add special palette for crop
$GLOBALS['TCA']['sys_file_reference']['palettes']['cropPalette'] = array(
	'showitem' => 'crop_ratio, crop_width, --linebreak--, crop_data',
	'canNotCollapse' => TRUE
);

	// Add palette to tt_content image fields foreign_types.
if ( is_array($GLOBALS['TCA']['tt_content']['columns']['image']['config']['foreign_types']) ) {
	foreach ($GLOBALS['TCA']['tt_content']['columns']['image']['config']['foreign_types'] as $key => $value) {
		$GLOBALS['TCA']['tt_content']['columns']['image']['config']['foreign_types'][$key]['showitem'] .= ',--div--;Cropping,--palette--;;cropPalette';
	}
}

/*
// Not implemented yet, needs to extend extbase too.
if ( is_array($GLOBALS['TCA']['tx_news_domain_model_news']['columns']['fal_media']['config']['foreign_types']) ) {
	foreach ($GLOBALS['TCA']['tx_news_domain_model_news']['columns']['fal_media']['config']['foreign_types'] as $key => $value) {
		$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['fal_media']['config']['foreign_types'][$key]['showitem'] .= ',--div--;Cropping,--palette--;;cropPalette';
	}
}
*/

if ( isset($GLOBALS['TCA']['sys_file_reference']['ctrl']['requestUpdate']) ) {
	$GLOBALS['TCA']['sys_file_reference']['ctrl']['requestUpdate']= 'crop_ratio';
} else {
	$GLOBALS['TCA']['sys_file_reference']['ctrl']['requestUpdate'] .= ',crop_ratio';
}
$GLOBALS['TCA']['sys_file_reference']['ctrl']['dividers2tabs']= 1;

?>